<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = [
            [
                'name'      => 'John Doe',
                'email'     => 'john@mail.com',
                'twitter'   => 'johndoe'
            ],
            [
                'name'      => 'Tailor Otwell',
                'email'     => 'tailor@mail.com',
                'twitter'   => 'tailorott'
            ],
            [
                'name'      => 'Steve Schoger',
                'email'     => 'steve@mail.com',
                'twitter'   => 'steveschoger',
            ],
        ];

        $list_users = User::get();

        return view('users.index', [
            'users' => $users,
            'list_users' => $list_users
        ]);
    }
    public function show($user)
    {
        return view('users.show', [
            'user' => $user,
        ]);
    }

}
