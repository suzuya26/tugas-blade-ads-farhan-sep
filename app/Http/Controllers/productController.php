<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class productController extends Controller
{
    public function indexPerStore($store){
        $toko = Store::find($store);
        $list_products = $toko->storeProducts;

        // return view('products.store.index',[
        //     'toko'=>$toko,
        //     'list_products' =>$list_products
        // ]);

        if(request()->ajax()){
            return DataTables::of($list_products)
            ->addIndexColumn()
            ->addColumn('prod_img',function($item){
                $gambar =
                '
                <img src='.$item->photo.' style="max-width: 100%"/>
                ';
                return $gambar;
            })
            ->addColumn('action',function($item){
                $render =
                '
                <a type="button" href="/products/update/'.$item->id.'" class="btn btn-primary">Update</a>
                ';
                return $render;
            })
            ->addColumn('delete',function($item){
                $render =
                '
                <a type="button" href="/products/delete/'.$item->id.'" class="btn btn-danger">Delete</a>
                ';
                return $render;
            })
            ->rawColumns(['prod_img', 'action','delete'])
            ->make(true);
        }

        return view('products.store.index',[
            'toko'=>$toko,
        ]);
    }

    public function pageAddProd($idstore){
        $ids = $idstore;
        return view('products.create',[
            'ids' => $ids
        ]);
    }

    public function addProduct(Request $request){
        $toko = Store::find($request -> store_id);
        $newproduk = $toko->storeProducts()->create([
            'name' => $request -> name,
            'slug' => $request -> slug,
            'price' => $request -> price,
            'description' => $request -> description,
            'photo' => $request -> photo,
        ]);
        return redirect('/products/'.$request -> store_id);
    }

    public function pageUpdateProd($idproducts){
        $product = Product::find($idproducts);
        return view('products.update',[
            'product' => $product
        ]);
    }

    public function updateProduct(Request $request,$id){
        Product::find($id)->update([
            'name' => $request->name,
            'slug' => $request->slug,
            'price' => $request->price,
            'description' => $request->description,
        ]);
        return redirect('/products/'.$request -> store_id);
    }

    public function destroyProduct($id){
        $product = Product::find($id);
        $store = $product['store_id'];
        $product->delete();
        return redirect('/products/'.$store);
    }
}
