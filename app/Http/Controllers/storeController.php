<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class storeController extends Controller
{
    public function index(){
        $store = Store::get();
        // return view('store.index',[
        //     'store' => $store
        // ]);

        if(request()->ajax()){
            return DataTables::of($store)
            ->addIndexColumn()
            ->addColumn('user_name',function($item){
                return $item ->user->name;
            })
            ->addColumn('action',function($item){
                $render =
                '
                <a type="button" href="products/'.$item->id.'" class="btn btn-success">Product</a>
                ';
                return $render;
            })
            ->addColumn('new_prod',function($item){
                $render =
                '
                <a type="button" href="products/new/'.$item->id.'" class="btn btn-primary">Tambah</a>
                ';
                return $render;
            })
            ->rawColumns(['action', 'new_prod'])
            ->make(true);
        }

        return view('store.index');
    }
}
