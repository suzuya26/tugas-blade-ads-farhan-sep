<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'price',
        'photo',
        'description',
        'store_id',
    ];

    public function seller()
    {
        return $this->belongTo(Store::class);
    }
}
