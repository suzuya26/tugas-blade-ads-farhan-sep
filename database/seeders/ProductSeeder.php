<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 27; $i++) {
            DB::table('products')->insert([
                'store_id'    => rand(1, 13),
                'name'      => $faker->sentence(4),
                'slug'      => $faker->sentence(4),
                'price'      => $faker->randomFloat(3, 1),
                'description' => $faker->sentence(100),
                'photo'       => $faker->imageUrl(1280, 720,'cats'),
                'created_at' => $created_at = now()->subDays(rand(1, 100)),
                'updated_at' => $created_at
            ]);
        }
    }
}
