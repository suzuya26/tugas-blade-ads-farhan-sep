<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class storeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
            [
                'user_id' => 1,
                'name' => 'toko bobi',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => 2,
                'name' => 'toko robi',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => 3,
                'name' => 'toko andi',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => 4,
                'name' => 'toko febi',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => 5,
                'name' => 'toko kroni',
                'created_at' => now(),
                'updated_at' => now()
            ],

        ]);
    }
}
