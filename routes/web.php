<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\productController;
use App\Http\Controllers\storeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('home');
Route::get('/', HomeController::class )->name('home');

Route::get('/lending', fn () => view('lending'));
//Route::get('/tentang', fn () => view('tentang'));
Route::get('/tentang', [PageController::class, 'tentang']);
Route::get('/gallery', [PageController::class, 'gallery']);

Route::get('/about', fn() => 'About')->name('about');
Route::get('/contact', fn() => 'Contact')->name('contact');

Route::get('user',function(){
    return 'Hello '. request('name');
});

Route::get('user/{user}',function($user){
    return 'Hello '. $user;
});

Route::get('/pengguna/{user}', fn ($user) => "My name is {$user}")->whereAlphaNumeric('user');

Route::get('/user/{id}/{name}', function ($id, $name) {
    return "My id is {$id} and my name is {$name}";
   })->whereNumber('id')->whereAlpha('name');

Route::prefix('kucing')->group(function(){
    Route::get('anggora', fn() => 'Berbulu tebal dan halus');
    Route::get('persia',fn()=> 'pesek');
    Route::get('oyen',fn()=> 'Barbar');
    Route::get('{nama_kucing}', fn($nama_kucing) => 'Nama Kucing lo : '.$nama_kucing);
});

//route utk tugas 2 : ADSBE_002

Route::get('petarungs', function () {
    $petarungs = [

    ];

    return view('petarungs.index', [
        'petarungs' => $petarungs,
    ]);
});

Route::middleware('kmkey')->group(function () {
    //Route::get('/dashboard', fn () => 'Dashboard')->name('dashboard');
    Route::get('/dashboard', DashboardController::class);
});

Route::get('users', [UserController::class, 'index']);
Route::get('users/{user}', [UserController::class, 'show']);

Route::get('stores',[storeController::class,'index']);

Route::get('products/{store}',[productController::class,'indexPerStore'])->name('storeproducts');
Route::get('products/new/{idstore}',[productController::class,'pageAddProd']);
Route::get('products/update/{idproducts}',[productController::class,'pageUpdateProd']);
Route::post('products/new',[productController::class,'addProduct']);
Route::put('products/update/{id}',[productController::class,'updateProduct']);
Route::get('products/delete/{id}',[productController::class,'destroyProduct']);

