@extends('layouts.app-store')
@section('content')
    <div class="container">
        <x-card title='List Products'>
            <table id="store-table" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Toko</th>
                        <th>Nama Pemilik</th>
                        <th>List Product</th>
                        <th>Tambah Produk</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </x-card>
    </div>
@endsection
@push('js')
<script>
    $(document).ready(function(){
        var storedatatable;
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        $(function(){
            storedatatable = $('#store-table').DataTable({
                processing: true,
                serverSide: true,
                // order: [[2, 'desc']],
                // pageLength : 2,
                // lengthMenu: [2, 10, 50, 100],
                // pagingType: "simple",
                ajax: "{{url()->current()}}",
                columns : [
                    {data: 'DT_RowIndex'},
                    {data: 'name'},
                    {data: 'user_name'},
                    {data: 'action'},
                    {data: 'new_prod'},
                ],
            })
        })
    })
</script>
@endpush
