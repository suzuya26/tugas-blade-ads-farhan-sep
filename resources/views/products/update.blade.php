<x-app-layout title="Edit Produk">
    <div class="container">
        <x-card title="Edit Produk">
          <div class="card-body">
            <form method="POST" action="/products/update/{{$product['id']}}">
                @csrf
                @method('PUT')
                <div class="form-group my-2" >
                  <label for="namaProduk">Nama</label>
                  <input type="hidden" name="store_id" class="form-control" id="namaProduk" value="{{$product['store_id']}}">
                  <input type="text" name="name" class="form-control" id="namaProduk" value="{{$product['name']}}">
                </div>
                <div class="form-group my-2">
                  <label for="slugProduk">Slug</label>
                  <input type="text" name="slug" class="form-control" id="slugProduk" value="{{$product['slug']}}">
                </div>
                <div class="form-group my-2">
                    <label for="priceProduk">Price</label>
                    <input type="number" name="price" class="form-control" id="priceProduk" value="{{$product['price']}}">
                </div>
                <div class="form-group my-2">
                    <label for="descriptionProduk">Description</label>
                    <input type="text" name="description" class="form-control" id="descriptionProduk" value="{{$product['description']}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
        </x-card>
    </div>
</x-app-layout>
