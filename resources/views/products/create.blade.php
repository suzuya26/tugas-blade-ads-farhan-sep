<x-app-layout title="Add Produk">
    <div class="container">
        <x-card title="Form Nambah Produk">
          <div class="card-body">
            <form method="POST" action="/products/new">
                @csrf
                <input type="hidden" name='store_id' class="form-control" id="namaProduk" value="{{$ids}}">
                <div class="form-group my-2" >
                  <label for="namaProduk">Nama</label>
                  <input type="text" name="name" class="form-control" id="namaProduk" placeholder="Nama produk">
                </div>
                <div class="form-group my-2">
                  <label for="slugProduk">Slug</label>
                  <input type="text" name="slug" class="form-control" id="slugProduk" placeholder="Slug Produk">
                </div>
                <div class="form-group my-2">
                    <label for="priceProduk">Price</label>
                    <input type="number" name="price" class="form-control" id="priceProduk" placeholder="harga Produk">
                </div>
                <div class="form-group my-2">
                    <label for="descriptionProduk">Description</label>
                    <input type="text" name="description" class="form-control" id="descriptionProduk" placeholder="Deskripsi Produk">
                </div>
                <div class="form-group my-2">
                    <label for="photoProduk">Photo</label>
                    <input type="text" name="photo" class="form-control" id="photoProduk" placeholder="Photo Produk, skrg bisa url saja">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
        </x-card>
    </div>
</x-app-layout>
