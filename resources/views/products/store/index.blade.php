<x-app-layout title="Products">
    <div class="container">
        <x-card title='List Products'>
            <table id="store-product-table" class="table">
                <h6>Form : {{$toko['name']}}</h6>
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Photo</th>
                    <th>Update</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                    {{-- @forelse ($list_products as $key => $lp)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $lp['name'] }}</td>
                            <td>{{ $lp['slug'] }}</td>
                            <td>{{ $lp['price'] }}</td>
                            <td>{{ $lp['description'] }}</td>
                            <td><img src="{{ $lp['photo'] }}" style="max-width: 100%"/></td>
                            <td><a href='/products/update/{{$lp['id']}}'>Update</a></td>
                            <td><a href='/products/delete/{{$lp['id']}}'>Delete</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class='text-center'>
                                    Data not found
                                </div>
                            </td>
                        </tr>
                    @endforelse --}}
                </tbody>
            </table>
        </x-card>
    </div>
    <script>
        $(document).ready(function(){
            var storedatatable;
            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $(function(){
                storedatatable = $('#store-product-table').DataTable({
                    processing: true,
                    serverSide: true,
                    // order: [[2, 'desc']],
                    // pageLength : 2,
                    // lengthMenu: [2, 10, 50, 100],
                    // pagingType: "simple",
                    ajax: "{{url()->current()}}",
                    columns : [
                        {data: 'DT_RowIndex'},
                        {data: 'name'},
                        {data: 'slug'},
                        {data: 'price'},
                        {data: 'description'},
                        {data: 'prod_img'},
                        {data: 'action'},
                        {data: 'delete'},
                    ],
                })
            })
        })
    </script>
</x-app-layout>
