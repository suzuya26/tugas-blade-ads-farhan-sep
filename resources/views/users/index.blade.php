<x-app-layout title="Users">
    <div class="container">
        <x-card title='Users dg data dari array di controller'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Twitter</th>
                </thead>
                <tbody>
                    @forelse ($users as $key => $user)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td><a href='/users/{{$user['name']}}' >{{ $user['name'] }}</a></td>
                        <td>{{ $user['email'] }}</td>
                        <td>{{ $user['twitter'] }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">
                            <div class='text-center'>
                                Data not found
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </x-card>
        <x-card title='Users dg data dari dari model'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>City</th>
                </thead>
                <tbody>
                    @forelse ($list_users as $key => $user)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td><a href='/users/{{$user['name']}}' >{{ $user['name'] }}</a></td>
                        <td>{{ $user['email'] }}</td>
                        <td>{{ $user['city'] }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">
                            <div class='text-center'>
                                Data not found
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </x-card>
    </div>
</x-app-layout>
