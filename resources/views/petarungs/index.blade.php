<x-app-layout title="Petarungs">
    <div class="container">
        <x-card title='Petarungs dengan metode foreach'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Twitter</th>
                    <th>Skill</th>
                </thead>
                <tbody>
                    @foreach ($petarungs as $key => $petarung)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $petarung['name'] }}</td>
                            <td>{{ $petarung['email'] }}</td>
                            <td>{{ $petarung['twitter'] }}</td>
                            <td>{{ $petarung['skill'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </x-card>
        <x-card title='Petarungs dengan kondisi if'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Twitter</th>
                    <th>Skill</th>
                </thead>
                <tbody>
                    @if (count($petarungs))
                        @foreach ($petarungs as $key => $petarung)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $petarung['name'] }}</td>
                                <td>{{ $petarung['email'] }}</td>
                                <td>{{ $petarung['twitter'] }}</td>
                                <td>{{ $petarung['skill'] }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">
                                <div class='text-center'>
                                    Data not found dengan metode if dipanggil dari else ny
                                </div>
                            </td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </x-card>
        <x-card title='Petarungs dengan metode empty'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Twitter</th>
                    <th>Skill</th>
                </thead>
                <tbody>
                    @empty(!$petarungs)
                        @foreach ($petarungs as $key => $petarung)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $petarung['name'] }}</td>
                                <td>{{ $petarung['email'] }}</td>
                                <td>{{ $petarung['twitter'] }}</td>
                                <td>{{ $petarung['skill'] }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">
                                <div class='text-center'>
                                    Data not found ini dengan empty
                                </div>
                            </td>
                        </tr>

                    @endempty
                </tbody>
            </table>
        </x-card>
        <x-card title='Petarungs dengan metode forelse'>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Twitter</th>
                    <th>Skill</th>
                </thead>
                <tbody>
                    @forelse ($petarungs as $p)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $p['name'] }}</td>
                            <td>{{ $p['email'] }}</td>
                            <td>{{ $p['twitter'] }}</td>
                            <td>{{ $p['skill'] }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class='text-center'>
                                    Data not found ini dengan forelse chek ny
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </x-card>
    </div>
</x-app-layout>
