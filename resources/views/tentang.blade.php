<x-app-layout title="Tentang">
    <div class="container">
        <h1>Tentang</h1>
        <x-card title='Laravel Framework' subtitle='Laravel is my PHP Framework'>
            Laravel is a web application framework with expressive, elegant syntax. We’ve
            already laid the foundation — freeing you to create without sweating the small things.
        </x-card>
    </div>
</x-app-layout>
